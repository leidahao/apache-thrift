package com.server;

import com.thrift.Person;
import com.thrift.PersonService;
import org.apache.thrift.TException;

/**
 * @Author : leih
 * @Date : 2020/4/14 23:30
 * @Version : 1.0
 * @Description : TODO
 * @ClassName : PersonServiceImpl
 */
public class PersonServiceImpl implements PersonService.Iface {
    @Override
    public Person getPersonByUsername(String username) throws TException {
        System.out.println("用户名:" +username);

        Person person = new Person();
        person.setAge(23);
        person.setDate("2018/02/02");
        person.setUsername("雄安");
        return person;
    }

    @Override
    public void savePerson(Person person) throws TException {
        System.out.println(person.getAge());
        System.out.println(person.getDate());
        System.out.println(person.getUsername());
    }
}
