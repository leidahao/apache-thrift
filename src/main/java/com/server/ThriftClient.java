package com.server;

import com.thrift.Person;
import com.thrift.PersonService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 * @Author : leih
 * @Date : 2020/4/14 23:44
 * @Version : 1.0
 * @Description : TODO
 * @ClassName : ThriftClient
 */
public class ThriftClient {

    public static void main(String[] args) {
        TTransport tTransport = new TFastFramedTransport(new TSocket("localhost",8899),600);
        TProtocol protocol = new TCompactProtocol(tTransport);
        PersonService.Client client = new PersonService.Client(protocol);

        try {
            tTransport.open();
            Person person = client.getPersonByUsername("xiaozheng");
            System.out.println(person.getUsername());
            System.out.println(person.getDate());
            System.out.println(person.getAge());

            Person person1  = new Person();
            person1.setUsername("person1");
            client.savePerson(person1);

        }catch (Exception e){

        }finally {
            tTransport.close();
        }

    }
}
